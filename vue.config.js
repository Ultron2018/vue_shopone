module.exports = {
  lintOnSave: false,
  configureWebpack: {
    devtool: 'source-map'
  }
  // chainWebpack: config => {
  //   config.when(process.env.NODE_ENV === 'production', config => {
  //     config.entry('app').clear().add('./src/main-prod.js')
  //   })

  //   config.when(process.env.MODE_ENV === 'development', config => {
  //     config.entry('app').clear().add('./src/main-dev.js')
  //   })
  // }
}
